<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    require("connexion.php");
    $connexion = connect_bd();

    function maxIdfilm($connexion){
        $res = 0;
        $nbfilms = $connexion->query('SELECT MAX(code_film) FROM films');
        foreach ($nbfilms as $f) {
            $res = $f['MAX(code_film)'];
        }
        return $res;
    }
    function ajouter_film($connexion){
        $insert = 'INSERT INTO `films` (`code_film`, `titre_original`, `titre_francais`,
        `pays`, `date`, `duree`, `couleur`, `realisateur`, `image`)
        values(:code_film, :titre_original, :titre_francais, :pays, :date,
        :duree, :couleur, :realisateur, :image);';
        $stmt = $connexion -> prepare($insert);

        $codeF = maxIdfilm($connexion)+1;
        $stmt->bindParam(':code_film', $codeF);
        $stmt->bindParam(':titre_original', $_GET['to']);
        $stmt->bindParam(':titre_francais', $_GET['tf']);
        $stmt->bindParam(':pays', $_GET['p']);
        $stmt->bindParam(':date', $_GET['date']);
        $stmt->bindParam(':duree', $_GET['duree']);
        $stmt->bindParam(':couleur', $_GET['couleur']);
        $stmt->bindParam(':realisateur', $_GET['rea']);
        $stmt->bindParam(':image', $_GET['img']);
        $stmt->execute();
    }

    ajouter_film($connexion);

    $result = $connexion->query('SELECT code_film, titre_francais FROM films where code_film > 560');
    foreach ($result as $m) {
      echo "<br>\n $m[titre_francais] $m[code_film]";
    }
    $connexion = null;
    ?>
</body>
</html>