<?php
require("modele.php");
$pays = get_pays();
$realisateur = get_realisateur();
$genre = get_genres();
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap.css">
    <title>Document</title>
</head>
<body style="background-color: rgb(0,0,0);">
  <div class="container-fluid" style="background-color: #B22222;">
    <div class="row">
      <h1 class="display-3 pl-5">Ajouter un film</h1>
    </div>
  </div>
  <div class="container mt-5 rounded pb-2 pt-2" style="background-color: white;">
    <form class="form" action="AccueilFilm.php" method="GET">
      <input type="hidden" name="cache" value="ajout">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="tf">
      </div>
      <div class="form-group">
        <label for="pays">Pays</label>
        <select class="custom-select" name="pays" id="pays">
          <?php
          foreach ($pays as $p) {
            echo "<option value=$p[code_pays]>$p[nom_pays]</option>";
          }
           ?>
        </select>
      </div>
      <div class="form-group">
        <label for="date">Date</label>
        <input type="text" name="date" id="date" class="form-control">
      </div>
      <div class="form-group">
        <label for="duree">Durée</label>
        <input type="text" name="duree" id="duree" class="form-control">
      </div>
      <div class="form-group">
        <label for="rea">Réalisateur</label>
        <input type="text" name="realisateur" id="rea" class="form-control">
      </div>
      <div class="form-group">
        <label for="genre">Genre</label>
        <select class="custom-select" name="genre">
          <?php
          foreach ($genre as $g) {
            echo "<option value=$g[code_genre]>$g[nom_genre]</option>";
          }
           ?>
        </select>
      </div>
      <div class="form-group">
        <label for="img">Image</label>
        <input type="text" name="img" id="img" class="form-control">
      </div>
      <input type="submit" value="Ajouter le film" class="btn btn-primary">
    </form>
  </div>

</body>
</html>
