-- phpMyAdmin SQL Dump
-- version 4.4.2
-- http://www.phpmyadmin.net
--
-- Client :  servinfo-db
-- Généré le :  Ven 17 Avril 2015 à 10:58
-- Version du serveur :  5.5.41-MariaDB-1ubuntu0.14.04.1
-- Version de PHP :  5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `dbgerard`
--

-- --------------------------------------------------------

--
-- Structure de la table `individus`
--

DROP TABLE IF EXISTS `individus`;
CREATE TABLE IF NOT EXISTS `individus` (
  `code_indiv` int(11) NOT NULL,
  `nom` varchar(40) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=892 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `individus`
--

INSERT INTO `individus` (`code_indiv`, `nom`) VALUES
(1,'Edouard Bergeon');


--
-- Index pour les tables exportées
--

--
-- Index pour la table `individus`
--
ALTER TABLE `individus`
  ADD PRIMARY KEY (`code_indiv`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `individus`
--
ALTER TABLE `individus`
  MODIFY `code_indiv` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=892;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
