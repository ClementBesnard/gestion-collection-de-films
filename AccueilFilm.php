<?php
require("modele.php");
if (isset($_GET['tf']) and isset($_GET['pays']) and isset($_GET['date']) and isset($_GET['duree']) and isset($_GET['realisateur']) and isset($_GET['genre']) and isset($_GET['img'])){
  if ($_GET['cache'] == 'ajout'){
    ajouter_film($_GET['tf'],$_GET['pays'], $_GET['date'], $_GET['duree'], $_GET['realisateur'],$_GET['genre'], $_GET['img']);
  }
}
if (isset($_GET['cache'])){
  if ($_GET['cache'] == 'suppr'){
    supprimer_film($_GET['codef']);
  }
}

$genres = get_genres();
$annees = get_annees();
// on test si les variables existent
if (isset($_GET['genre']) and isset($_GET['date']) and isset($_GET['tf'])){
  $films  = get_films(0,0);
}
elseif (isset($_GET['genre']) and isset($_GET['annee'])){
  $films  = get_films($_GET['genre'], $_GET['annee']);
}
elseif (isset($_GET['genre']) and !isset($_GET['annee'])) {
  $films  = get_films($_GET['genre'], 0, 0);
}
elseif (!isset($_GET['genre']) and isset($_GET['annee'])) {
  $films  = get_films(0, $_GET['annee'], 0);
}
else {
  if (isset($_GET['recherche']))
    $films = rechercheFilm($_GET['recherche']);
  else {
    $films  = get_films(0,0);
  }
}


?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="style.css">
    <title>Gestion Films</title>
</head>
<body>
    <div class="container-fluid topnav">
        <div class="row ">
            <div class="col-md-4 form-group">
              <form action="AccueilFilm.php" method="get">
                <input type="text" name="recherche" class="form-control-sm">
                <input type="submit" name="button" value="Recherche">

              </form>

            </div>

            <div class="col-md-5 form-group">
              <div class="row">
                <div class="col-md-6">
                  <form action="AccueilFilm.php" method="get">
                    <select name="genre" class="form-control-sm">
                        <option value="default" >Genre</option>
                        <?php

                        foreach ($genres as $g)
                        {
                            echo "<option value=$g[code_genre]>$g[nom_genre]</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="annee" class="form-control-sm">
                        <option value="annee">Année</option>
                        <?php
                        foreach ($annees as $a)
                        {
                            echo "<option value=$a[date]>$a[date]</option>";
                        }
                        ?>
                    </select>

                </div>
                <div class="col-md-3">
                  <input type="submit" value="Filtrer">
                </div>

                </form>

              </div>

            </div>
            <div class="col-md-3 form-group">
              <div class="row">
                <div class="col-md-4 ">
                  <form class="" action="ajouterfilm.php" method="post">
                    <input type="submit" value="Ajouter un film" class="form-control-sm">
                  </form>

                </div>
              </div>
            </div>

        </div>
      </div>
      <div class="container div-film">
        <div class="row text-center ">
          <?php
              foreach ($films as $f) {



                  echo "<div class='col film mh-100'>\n";
                  echo "<img src=$f[image]> <br>";
                  echo "</a>";
                  echo "$f[titre] <br>";
                  echo "De $f[nom] <br>";
                  echo "Sortie en $f[date]<br>";
                  echo "<form class='form' action='AccueilFilm.php' method='GET'>";
                  echo "<input type='hidden' name='codef' value= $f[code_film]>";
                  echo '<input type="hidden" name="cache" value="suppr">';
                  echo "<input type='submit' value='Supprimer'>";
                  echo "</div>";
                  echo "</form>";
              }

          ?>
        </div>
      </div>



</body>
</html>
