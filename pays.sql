SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `code_pays` int(11) NOT NULL,
  `nom_pays` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8;

INSERT INTO `pays` (`code_pays`,`nom_pays`) VALUES
(1,'USA' ),
(2,'Italie'),
(3,'Allemagne'),
(4,'France'),
(5,'Suède'),
(6,'Canada'),
(7,'Chine'),
(8,'Angleterre '),
(9,'Japon'),
(10,'Mexique'),
(11,'Danemark');

ALTER TABLE `pays`
  ADD PRIMARY KEY (`code_pays`);
