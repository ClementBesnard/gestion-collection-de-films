
drop table FILM;

create table FILM(
    Titre Varchar2(40);
    Realisateur Varchar2(40);
    AnneeRealisation date;
    Genre Varchar2(20);
    constraint KTitre primary key (Titre)
);