<?php
require("connexion.php");

function get_pays(){
  $connexion = connect_bd();
  $liste_pays = array();
  $pays = $connexion->query('SELECT  code_pays,nom_pays from pays');
  foreach ($pays as $p) {
    array_push($liste_pays,$p);
  }
  return $liste_pays;
}

function get_realisateur(){
  $connexion = connect_bd();
  $liste_realisateur = array();
  $realisateur = $connexion->query('SELECT distinct nom,code_indiv from individus order by nom asc');
  foreach ($realisateur as $r) {
    array_push($liste_realisateur,$r);
  }
  return $liste_realisateur;
}


function get_genres(){
  $connexion = connect_bd();
  $liste_genres = array();
  $genres = $connexion->query('SELECT code_genre,nom_genre from genres');
  foreach ($genres as $g) {
    array_push($liste_genres,$g);
  }
  return $liste_genres;
}

function get_annees(){
  $connexion = connect_bd();
  $liste_annees = array();
  $annees = $connexion->query('SELECT distinct date from
  films order by date desc');
  foreach ($annees as $a) {
    array_push($liste_annees,$a);
  }
  return $liste_annees;
}

function get_films($genre,$annee){
  $connexion = connect_bd();
  $liste_films = array();
  if ($genre == 0 and $annee == 0){
    $films = $connexion->query('SELECT code_film,titre,nom,date,image FROM films
  natural join individus where realisateur=code_indiv');
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }
  elseif ($genre == 0) {
    $films = $connexion->query("SELECT distinct code_film,titre,nom,date,image from films
    natural join classification natural join individus where realisateur=code_indiv and code_film=ref_code_film and date=$annee");
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }
  elseif ($annee == 0) {
    $films = $connexion->query("SELECT distinct code_film,titre,nom,date,image from films
    natural join classification natural join individus where realisateur=code_indiv and code_film=ref_code_film and ref_code_genre=$genre");
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }
  else {
    $films = $connexion->query("SELECT distinct code_film,titre,nom,date,image from films
    natural join classification natural join individus where realisateur=code_indiv and code_film=ref_code_film and ref_code_genre=$genre and date=$annee");
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }
}

function rechercheFilm($chaine){
  $connexion = connect_bd();
  $liste_films = array();
  if ($chaine == ''){
    $films = $connexion->query('SELECT code_film,titre,nom,date,image FROM films
    natural join individus where realisateur=code_indiv');
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }
  else {
    /*$chaine[0] = ucwords($chaine[0]);*/
    $films = $connexion->query("SELECT distinct code_film,titre,nom,date,image from films
    natural join individus where realisateur=code_indiv and ucase(titre) like ucase('%$chaine%')");
    foreach ($films as $f) {
      array_push($liste_films,$f);
    }
    return $liste_films;
  }

}

function maxIdfilm(){
    $connexion = connect_bd();
    $res = 0;
    $nbfilms = $connexion->query('SELECT MAX(code_film) FROM films');
    foreach ($nbfilms as $f) {
        $res = $f['MAX(code_film)'];
    }
    return $res;
}

function maxIdIndiv(){
    $connexion = connect_bd();
    $res = 0;
    $nbindiv = $connexion->query('SELECT MAX(code_indiv) FROM individus');
    foreach ($nbindiv as $i) {
        $res = $i['MAX(code_indiv)'];
    }
    return $res;
}

function ajouter_film($tf,$p,$date,$duree,$rea,$genre,$img){
    $connexion = connect_bd();
    $insert1 = 'INSERT INTO `films` (`code_film`,`titre`,
    `pays`, `date`, `duree`,`realisateur`, `image`)
    values(:code_film, :titre, :pays, :date,
    :duree,:realisateur, :image);';

    $insert2 = 'INSERT INTO `classification` (`ref_code_film`,`ref_code_genre`)
    values(:code_film, :code_genre);';

    $insert3 = 'INSERT INTO `individus` (`code_indiv`,`nom`)
    values(:code_indiv, :nom);';

    $stmt = $connexion -> prepare($insert1);
    $stmt2 = $connexion -> prepare($insert2);
    $stmt3 = $connexion -> prepare($insert3);
    $tf[0] = ucwords($tf[0]);
    $codeF = maxIdfilm($connexion)+1;
    $codeI = maxIdIndiv($connexion)+1;
    $stmt->bindParam(':code_film', $codeF);
    $stmt->bindParam(':titre', $tf);
    $stmt->bindParam(':pays', $p);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':duree', $duree);
    $stmt->bindParam(':realisateur', $codeI);
    $stmt->bindParam(':image', $img);
    $stmt2->bindParam(':code_film',$codeF);
    $stmt2->bindParam(':code_genre',$genre);
    $stmt3->bindParam(':code_indiv',$codeI );
    $stmt3->bindParam(':nom',$rea);

    $stmt->execute();
    $stmt2->execute();
    $stmt3->execute();

}

function supprimer_film($codef){
  $connexion = connect_bd();
  $sql = "DELETE FROM films WHERE code_film = '$codef'";
  $connexion->exec($sql);
}

 ?>
